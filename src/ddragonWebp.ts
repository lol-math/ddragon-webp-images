import { transformUrl } from "./transformUrl";

export const ddragonWebp = {
  /**
   * The URL of your CDN.
   *
   * Defaults to "https://ddragon-webp.lolmath.net"
   * which will normally be on the latest version.
   *
   * No trailing slash.
   */
  baseurl: "https://ddragon-webp.lolmath.net",
  /**
   * Gets the splash image of a champion + skin
   */
  splash: (name: string, num: number) =>
    `${ddragonWebp.baseurl}/img/champion/splash/${
      name === "Fiddlesticks" ? "FiddleSticks" : name
    }_${num}.webp`,
  /**
   * Gets the loading image of a champion + skin
   */
  loading: (name: string, num: number) =>
    `${ddragonWebp.baseurl}/img/champion/loading/${
      name === "Fiddlesticks" ? "FiddleSticks" : name
    }_${num}.webp`,
  /**
   * Get the champion tile of a champion + skin
   */
  tile: (name: string, num: number) =>
    `${ddragonWebp.baseurl}/img/champion/tiles/${
      name === "Fiddlesticks" ? "FiddleSticks" : name
    }_${num}.webp`,
  /**
   * Get the champion centered image of a champion + skin
   */
  centered: (name: string, num: number) =>
    `${ddragonWebp.baseurl}/img/champion/centered/${
      name === "Fiddlesticks" ? "FiddleSticks" : name
    }_${num}.webp`,
  /**
   * A champion square
   */
  champion: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/champion/${full}`),
  item: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/item/${full}`),
  map: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/map/${full}`),
  mission: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/mission/${full}`),
  passive: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/passive/${full}`),
  profileicon: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/profileicon/${full}`),
  spell: (full: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/spell/${full}`),
  summoner: (full: string) => ddragonWebp.spell(full),
  sprite: (sprite: string) =>
    transformUrl(`${ddragonWebp.baseurl}/latest/img/sprite/${sprite}`),
  rune: (icon: string) => transformUrl(`${ddragonWebp.baseurl}/img/${icon}`),
  /**
   * A stat rune
   */
  statMod: (statName: string) => {
    return `${ddragonWebp.baseurl}/img/perk-images/StatMods/StatMods${statName}Icon.webp`;
  },
};
