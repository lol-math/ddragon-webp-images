/**
 * replaces the JPEG or PNG url with a webp version.
 *
 * @param url the JPEG or PNG from the riot API
 */
export const transformUrl = (url: string) => {
  return url.replace(/.(png|jpg|jpeg)$/, ".webp");
};
