import { ddragonWebp } from "../src";

describe("ddragon webp", () => {
  it("should have a base url", () => {
    expect(ddragonWebp.baseurl).toBeDefined();
  });
  it("should use the baseurl when set", () => {
    ddragonWebp.baseurl = "https://example.com";
    expect(ddragonWebp.champion("Aatrox.png")).toContain("https://example.com");
  });
  it("should create a valid URL", () => {
    ddragonWebp.baseurl = "https://example.com";
    expect(ddragonWebp.champion("Aatrox.png")).toEqual(
      "https://example.com/latest/img/champion/Aatrox.webp"
    );
  });
});
