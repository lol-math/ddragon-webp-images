import { transformUrl } from "../src/transformUrl";

describe("Transform URL", () => {
  it("returns a string", () => {
    expect(typeof transformUrl("https://example.com/test/image.png")).toBe(
      "string"
    );
  });
  it("replaces PNG at the end of a URL with webp", () => {
    expect(transformUrl("https://example.com/test/image.png").substr(-5)).toBe(
      ".webp"
    );
  });
  it("replaces JPEG urls with WEBP", () => {
    expect(transformUrl("https://example.com/test/image.jpg").substr(-5)).toBe(
      ".webp"
    );
    expect(transformUrl("https://example.com/test/image.jpeg").substr(-5)).toBe(
      ".webp"
    );
  });
});
