## [1.1.2](https://gitlab.com/lol-math/ddragon-webp-images/compare/v1.1.1...v1.1.2) (2023-11-16)


### Bug Fixes

* use normal name again as riot has fixed magic resist icon in dragontail ([34e1f2a](https://gitlab.com/lol-math/ddragon-webp-images/commit/34e1f2add7db4a8a93803f91df830e994b5a6028))

## [1.1.1](https://gitlab.com/lol-math/ddragon-webp-images/compare/v1.1.0...v1.1.1) (2022-11-20)


### Bug Fixes

* add magic res fix specific fallback ([9e8309d](https://gitlab.com/lol-math/ddragon-webp-images/commit/9e8309dca4f29a9c9ede10e30202e4d583f4d1c3))

# [1.1.0](https://gitlab.com/lol-math/ddragon-webp-images/compare/v1.0.4...v1.1.0) (2022-11-07)


### Features

* centered image ([f9034fd](https://gitlab.com/lol-math/ddragon-webp-images/commit/f9034fd7a735426832ad170ab622531981a73d7f))

## [1.0.4](https://gitlab.com/lol-math/ddragon-webp-images/compare/v1.0.3...v1.0.4) (2022-07-17)


### Bug Fixes

* re-add types using @parcel/transformer-typescript-types ([1945f4c](https://gitlab.com/lol-math/ddragon-webp-images/commit/1945f4cefeab5ac5a9f8da5d5f7f8cc874722338))

## [1.0.3](https://gitlab.com/lol-math/ddragon-webp-images/compare/v1.0.2...v1.0.3) (2022-07-16)


### Bug Fixes

* make sure we use @semantic-release/git to update the package.json version (and add changelog) ([784bd96](https://gitlab.com/lol-math/ddragon-webp-images/commit/784bd96b73553fe6932172a1db9c18dd48b8e285))
